<?php
$page_id = 'signup.php';
include 'inc/header.php';
?>



		<div class="container hero_spot"><!-- start of #hero_spot --> 
				
		</div>
		<!-- end of #hero_spot --> 
</div>
<!-- end of #onstack01 -->

<div class="start_osTop_pgTop"></div>
<div class="start_osTop_pg"></div>
<!--<div class="start_os"></div>-->

<div class="container_home my-container"><!-- start of #stacking/explore -->
		<div class="row_os">
				<div class="col-md-3"> </div>
				<div class="col-md-3"> </div>
		</div>
</div>
</div>
<div class="col-md-3"> </div>
</div>
</div>
<!-- end #stacking/explore --> 

<!-- start of #my-container -->
<div class="container_home my-container" style="min-height:700px;"> 
		<!-- start of #my-container -->
		
		<div class='row_os' style="margin:0 auto;">
				<div class="col-md-6 topTstacks">Sign Up</div>
				<br>

				<!-- // -->
				
				<div class='col-md-10'>
						<form>
								<div class="form-group " id="signup">
										<label  for="firstname">First Name</label>
										<input type="text" class="form-control" id="firstname" placeholder="First Name" required>
										<label  for="lastname">Last Name</label>
										<input type="text" class="form-control" id="lastname" placeholder="Last Name">
										<label  for="email">Email</label>
										<input type="text" class="form-control" id="email" placeholder="Email">
										<label  for="password">Password</label>
										<input type="password" class="form-control" id="password" placeholder="Password">
										<label  for="confpassword">Confirm Password</label>
										<input type="password" class="form-control" id="confpassword" placeholder="Confirm Password">
										
										
								</div>
						
				</div>
			
				
				<hr>
				<div class='col-md-10'>
						<div  class="button"> 
								
								
								<button type="submit-bt"  class="btn btn-default" value="SignUp">Sign Up</button>
							
							</form>	
						</div>
						<div class=" line-separator"></div>
						<div id="or"><span id="or">Or</span></div>
				</div>
				
				
				<div class='col-md-6 col-lg-offset-2 col-md-offset-2'>
						<div class="button">
						<button type="submit" class="btn btn-primary  btn-block btn-facebook"><i class="fa fa-facebook"></i> Connect with Facebook</button>
								
								
						</div>
				</div>
				
				<!-- /.container --> 
		</div>
		
</div>
<!-- end of #my-container -->


     <!-- footer -->
 <?php include 'inc/footer.php'; ?>