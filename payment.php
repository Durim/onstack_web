<?php
$page_id = 'dashboard.php';
include 'inc/header_admin.php';
?>
       <div class="container hero_spot"><!-- start of #hero_spot -->
       
           
      </div><!-- end of #hero_spot -->
  </div><!-- end of #onstack01 -->
  
  <div class="start_osTop_pgTop"></div>  
   <div class="start_osTop_pg"></div>
  <!--<div class="start_os"></div>-->
  
<div class="container_home my-container"></div>

<div class="container_dashboard my-container"><!-- start of #my-container -->
      <div class="row">
        <div class="col-md-8_main">
        
        <span class="relTstacks">Payment</span><!-- #New Stack -->
        <br>
        <hr><br>

  <div class="col-md-3"></div>
  <div class="col-md-8">
                
             <input type="radio" name="anonymous" id="pay01" checked=""><label for="pay01" style="color:#86c5ec;">&nbsp;&nbsp;<i class="fa fa-cc-mastercard fa-3x"></i>&nbsp;<i class="fa fa-cc-visa fa-3x"></i></label>
              &nbsp;&nbsp;&nbsp;<input type="radio" name="anonymous" id="pay02"><label for="pay02" style="color:#86c5ec;">&nbsp;&nbsp;<i class="fa fa-cc-paypal fa-3x"></i></label>
               
              
         


  </div>

  <div class="col-md-12">
   <br><br>

  <form class="form-horizontal" role="form">
      <fieldset>
           <div class="form-group">
        <label class="col-md-3 control-label" for="card-holder-name">Name on Card</label>
        <div class="col-md-8">
          <input type="text" class="form-control" name="card-holder-name" id="card-holder-name" placeholder="Card Holder's Name">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label" for="card-number">Card Number</label>
        <div class="col-md-8">
          <input type="text" class="form-control" name="card-number" id="card-number" placeholder="Debit/Credit Card Number">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label" for="expiry-month">Expiration Date</label>
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-3" style="margin-left:-10px;">
              <select class="form-control col-md-3" name="expiry-month" id="expiry-month">
                <option>Month</option>
                <option value="01">Jan (01)</option>
                <option value="02">Feb (02)</option>
                <option value="03">Mar (03)</option>
                <option value="04">Apr (04)</option>
                <option value="05">May (05)</option>
                <option value="06">June (06)</option>
                <option value="07">July (07)</option>
                <option value="08">Aug (08)</option>
                <option value="09">Sep (09)</option>
                <option value="10">Oct (10)</option>
                <option value="11">Nov (11)</option>
                <option value="12">Dec (12)</option>
              </select>
            </div>
            <div class="col-md-3" style="margin-left:-10px;">
              <select class="form-control" name="expiry-year">
                <option value="13">2013</option>
                <option value="14">2014</option>
                <option value="15">2015</option>
                <option value="16">2016</option>
                <option value="17">2017</option>
                <option value="18">2018</option>
                <option value="19">2019</option>
                <option value="20">2020</option>
                <option value="21">2021</option>
                <option value="22">2022</option>
                <option value="23">2023</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label" for="cvv">Card CVV</label>
        <div class="col-md-3">
          <input type="text" class="form-control" name="cvv" id="cvv" placeholder="Security Code">
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
          <button type="button" class="btn btn-success">Order</button>
        </div>
      </div>
    </fieldset>
  </form>
  </div>

            
        </div><!-- end of #dashboard_cont -->
     
<div class="col-md-3_dash pull-right">
<div id="dashboard_right">
<div class="relTstacks">Billing Inof</div>        

        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
        <br><br>    
            
            </div>
            </div>
                
      </div><!--  end of #cont.  -->

  </div><!-- end of #my-container -->

     <!-- footer -->
 <?php include 'inc/footer.php'; ?>