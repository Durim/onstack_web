<!DOCTYPE html>
<html lang="en" hola_ext_inject="disabled"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/os_fav.ico" type=type="image/x-icon">


    <title>OnStack</title>


    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/onstack.css" rel="stylesheet">
    <link href="css/intro_test.css" rel="stylesheet">
    <script src="./js/ie-emulation-modes-warning.js"></script>
    <script src="./js/scroll.js"></script>
   <script src="//api.html5media.info/1.1.8/html5media.min.js"></script>
     <!--[if lt IE 9]>
    <p>Sorry! You are running IE 8 or an earlier version of IE. <br>Please use IE 9 or greater</p>
    <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->

   
   
   <!-- Test  -->
   <script src="./js/log.min.js"></script>
   
   <!-- end of #Test -->
   
   
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body onload="changeText();">
   <div id="ie">
 
<div class="onstack01"><!-- start of #onstack01 -->
  <div id="os_header" class="container">
  <div id="os_logo"><a href="home.php"><img src="images/logo.png" width="180" height="41"  alt=""/></a></div>
  <div id="os_socmedia"><a href="#"><img src="images/email.png" alt="" width="25" height="25"/></a>&nbsp;<a href="#"><img src="images/fb.png" alt="" width="25" height="25"/></a>&nbsp;<a href="#"><img src="images/tw.png" alt="" width="25" height="25"/></a></div>
  <div id="os_srch"><form id="srch">
  <input type="search" placeholder="Search Stacks" value="Search Stacks" onfocus="if(this.value  == 'Search Stacks') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Search Stacks'; } " />
</form></div>
  </div>
     
  <!-- Youtube video test -->
    <div class="test_feature"> 
   <div class="feature" style="height: 100%; opacity: 1;">
    <!--<h1 style="height: 110px; width: 1376px;"></h1>-->
    <video autoplay loop poster="video/video.jpg" id="bgvid" >
      <source src="video/exponent.mp4" type="video/mp4">
      <source src="video/exponent.ogv" type="video/ogg">
      <!--<img src="video/video.jpg" width="100%" height="100%" alt="" style="vertical-align:middle">-->
    </video>
</div>
  </div>

<!-- end of #Youtube video test -->
    
       <div class="container123 hero_spot"><!-- start of #hero_spot -->
       <div id="os_desc">
       <div class="onbg">
       <h2 class="classh2">OnStack</h2><h3 class="classh3">
      
     <script>
     
    var counter = 0;

        function changeText()
        {
        var quotes = new Array();

        quotes[0] = "Be part of Social Changes";
        quotes[1] = "Be Yourself";
        quotes[2] = "Sign It with you face";
        quotes[3] = "Be Real, No Filters";

        if (counter > 3)
            {
            counter = 0;
            }

        document.getElementById("textslide").innerHTML = quotes[counter];

        setTimeout(function(){changeText()},10000);
        counter ++;
        }
    
    
        </script>
        

        <p id="textslide"></p>
        </h3>
        
        </div>
        </div><!-- end of #os_desc -->
 
 
 
<!-- log -->

        
 <script> //Vimeo Test
            // Analytics Setup
        var _comscore = _comscore || [],
            _gaq = _gaq || [];

        _comscore.push({
            c1: "2",
            c2: "10348289"
        });

        _gaq.push(['_require', 'inpage_linkid', '//www.google-analytics.com/plugins/ga/inpage_linkid.js']);
        _gaq.push(['_setAccount', "UA-76641-8"]);
        _gaq.push(['_gat._anonymizeIp']);

                    _gaq.push(['_setDomainName', 'vimeo.com']);
        
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_setAllowAnchor', true]);

        
        function trackHouseAd() {
            var ga_l = document.location;
            var ga_r = document.referrer;
            var ga_s = ga_l.search;

            if (ga_s.indexOf('house_ad') > -1) {
                var house_ad_value = ga_s.match(/house_ad=([^&]+)/)[1];

                _gaq.push(['_trackEvent', 'engagement:house-ad', house_ad_value, ga_r, undefined, true]);
                _gaq.push(['_setCustomVar', 8, 'house_ad', house_ad_value, 1]);
            }
        }

        trackHouseAd();

        
        _gaq.push(['_setCustomVar', 1, 'user_status', 'logged_out', 3]);
        _gaq.push(['_setCustomVar', 4, 'language', 'en', 3]);
        _gaq.push(['_setCustomVar', 3, 'ms', '0', 1]);
        _gaq.push(['_setCustomVar', 10, 'vuid', document.cookie.replace(/(?:(?:^|.*;\s*)vuid\s*\=\s*([^;]*).*$)|^.*$/, "$1"), 1]);

        
        _gaq.push(['_trackPageview']);

        
            
    var CSS_DIR = 'https://f.vimeocdn.com/styles/css_opt/',
        JS_DIR = 'https://f.vimeocdn.com/js_opt/',
        IMAGE_DIR = 'https://f.vimeocdn.com/images_v6/',
                BUILD_HASH = '2a38d',
        vimeo = vimeo || {};

    vimeo.app_version = 'v6';
    vimeo.domain = '.vimeo.com';
    vimeo.url = 'vimeo.com';
    vimeo.xsrft = '686c9615d01587cb4d16918069148920.0';
    vimeo.cur_user = null;
    vimeo.config = [];
    vimeo.party_pooped = true;

    
    
    var localeConfig = {
        'Date': {
            months: ["January","February","March","April","May","June","July","August","September","October","November","December"],
            months_abbr: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
            days: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
            days_abbr: ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],

            // Culture's date order: MM/DD/YYYY
            dateOrder: ['date', 'month', 'year'],
            shortDate: '%d/%m/%Y',
            shortTime: '%I:%M%p',
            AM: 'AM',
            PM: 'PM',
            firstDayOfWeek: 0,

            // Date.Extras
            ordinal: function(dayOfMonth){
                return dayOfMonth;
            }
        },
        'DatePicker': {
            select_a_time: "Select a time",
            use_mouse_wheel: "Use the mouse wheel to quickly change value",
            time_confirm_button: "OK",
            apply_range: "Apply",
            cancel: "Cancel",
            week: "Wk"        },
                'Number': {
            decimal: '.',
            group: ',',
            currency: {
                prefix: '$'
            }
        },
        'FormValidator': {"required_edit":"This field is required.","requiredChk":"This field is required."}    };

    var fullLocale = '';

    
    var Copy = {
        translate: function(key, plural, replacements) {
            var translation = typeof this.dict[key] != 'object' ? this.dict[key] : (plural ? this.dict[key].plural : this.dict[key].singular);
            if (typeof replacements === 'object') {
                translation = this.substitute(translation, replacements);
            }
            return translation;
        },
        substitute: function(string, object) {
            if (!string) {
                return;
            }
            if (typeof string.substitute !== 'undefined') {
                return string.substitute(object);
            }
            else {
                return string.replace(/\\?\{([^{}]+)\}/g, function(match, name) {
                    if (match.charAt(0) == '\\') return match.slice(1);
                    return (object[name] != null) ? object[name] : '';
                });
            }
        },
        dict: {"did_you_mean_email":"Did you mean <em>{SUGGEST}<\/em>?","email_password_mismatch":"Email and password do not match","just_now":"just now","seconds_ago":{"singular":"{COUNT} second ago","plural":"{COUNT} seconds ago"},"minutes_ago":{"singular":"{COUNT} minute ago","plural":"{COUNT} minutes ago"},"hours_ago":{"singular":"{COUNT} hour ago","plural":"{COUNT} hours ago"},"open_comment_box":"Add new comment instead &raquo;","url_unavailable":"Sorry, this url is unavailable.","unsaved_changes_generic":"You have unsaved changes, are you sure you wish to leave?","add":"Add","remove":"Remove","select":"Select","no_followers_for_letter":"You don&rsquo;t follow anyone that begin with the letter &ldquo;{PAGE_LETTER}&rdquo;","share_limit_reached":"You have reached the maximum number of users to share with.","old_code":"Use old embed code","new_code":"Use new embed code","at_least_one":"There must be at least one user.","available":"Available","unavailable":"Unavailable","browse_error_generic":"Sorry, there was an error","browse_error_no_videos":"Sorry, no videos found","follow":"Follow","following":"Following","unfollow":"Unfollow","unfollowing":"Unfollowing","count_comments":{"singular":"{COUNT} comment","plural":"{COUNT} comments"},"first_comment":"Be the first to comment&hellip;","no_comments_for_you":"Forbidden. You cannot post comments on this page.","oops":"Oops!","player_try_again":"That wasn't supposed to happen. Please try again in a few minutes.","duration_input_min_duration":"The duration cannot be less than {MIN_DURATION}.","duration_input_max_duration":"The duration cannot be greater than {MAX_DURATION}.","duration_input_invalid_characters":"0-9 and : are the only acceptable inputs.","close":"Close","expand":"Expand","loading":"Loading...","top":"top","advanced_search":"Advanced Search","no_suggestions":"No suggestions","recent_searches":"Recent Searches","search_all":"Search All of Vimeo"}    };

// #####Vimeo Test
</script>


    


<!-- end of #log -->
 
 
       <div id="os_join_signup">
       <div class="onforma">
       <!-- start of #login form -->

            <div class="os_cont_log os_test_box">
                <div class="flip_box">
                    <div class="flip_box_card front sign_up_forms os_test_box_card">
                        <form class="row cta_form tooltipify_errors js-join_form" action="#" method="">
                            <input type="hidden" name="" value="">

                    <div id="signup01">                    
                    <input type="text" class="form-control" id="firstname" placeholder="First Name" required>                   
                    <input type="text" class="form-control" id="lastname" placeholder="Last Name" required>                   
                    <input type="text" class="form-control" id="email" placeholder="Email">                    
                    <input type="password" class="form-control" id="password" placeholder="Password">
                    </div>

                            <div class="row">
                                <input type="submit" id="join_btn" class="btn btn-default" value="Join">
                                <span class="alt_form_option"><a class="btn btn-success1 btn_log os_test_box_toggle js-login_toggle" href="#">Login</a></span>
                            </div>
                            <small class="row txt_md"><!--OnStack<a href="#">OS</a> &amp; <a href="#">Test</a>--></small>
                        <input name="token" type="hidden" value=""></form>
<br>
                        <form class="row" action="#" method="post" id="facebook_join">
                            <input type="hidden" name="service" value="facebook">
                            <button type="submit" class="fb_test">Join with Facebook</button>
                        <input name="token" type="hidden" value="ncuk"></form>
                    </div>

                    <div class="flip_box_card back log_in_forms os_test_box_card" style="height: 299px;">
                        <form class="row cta_form tooltipify_errors js-login_form" action="#" method="" id="login_form">
                            <input type="hidden" name="" value="">
                            <input type="hidden" name="" value="">
                                

                                    <div id="login01">
                                    <input type="text" class="form-control" id="email" placeholder="Email" required>                                   
                                    <input type="password" class="form-control" id="password" placeholder="Password" required>
                                    <div class="checkbox">
                                      <label>Forgot Password?</label>
                                    </div>
                                </div>


                            <div class="row">
                                <input type="submit" id="login_btn" class="btn btn-success" value="Log in">
                                <span class="alt_form_option"><a class="btn_log1 btn btn-default os_test_box_toggle js-join_toggle" href="#">Join</a></span>
                            </div>

                            <small class="row txt_md"><!--<a href="#">Forgot password?</a>--></small>
                        <input name="token" type="hidden" value=""></form>

                        <form class="row login en" action="#" method="post" id="facebook_form">
                            <input type="hidden" name="service" value="">
                            <input type="hidden" name="action" value="">
                            <button type="submit" class="fb_test">Log in with Facebook</button>
                        <input name="token" type="hidden" value=""></form>
                    </div>
                </div>
            </div>
 <!-- end of #login form -->
       </div>
       </div><!-- end of #os_join_signup -->
           
      </div><!-- end of #hero_spot -->
  </div><!-- end of #onstack01 -->
  
   <div class="start_osTop"></div>
  <div class="start_os">
  <div class="sstacking"><a class="scroll" href="#more"><p>Start Stacking</p></a></div>
  <div class="start_inner">
  <a class="scroll" href="#more"><img src="images/more.png" width="35" height="22"  alt=""/></a>
  </div>
  </div>
 <section id="more"> 
 <div class="container_home my-container">
 <a name="more"></a>
  <div id="os_howitworks">
     <a href="home.php"><span class="how">how</span><span class="it">it</span><span class="works">works</span></a></div>
  </div> 