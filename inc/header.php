<!DOCTYPE html>
<html lang="en" hola_ext_inject="disabled">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/os_fav.ico" type=type="image/x-icon">

    <title>OnStack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/onstack.css" rel="stylesheet">
    <script src="./js/ie-emulation-modes-warning.js"></script>
   <script src="//api.html5media.info/1.1.8/html5media.min.js"></script>
      <!--[if lt IE 9]>
<p>Sorry! You are running IE 8 or an earlier version of IE. <br>Please use IE 9 or greater</p>
<link rel="stylesheet" type="text/css" href="css/ie.css" />
<![endif]-->
   
   
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
 <div id="ie">
<div class="onstack_pg"><!-- start of #onstack_pg -->
  <div id="os_header_pg" class="container">
 <div id="os_Topmenu"><!-- start #menu -->

     <nav class="navbar navbar-default" role="navigation">
   <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" 
         data-target="#example-navbar-collapse">
         <span class="sr-only">Nav</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
      </button>
      <!--<a class="navbar-brand" href="#">/</a>-->
   </div>
   <div class="collapse navbar-collapse" id="example-navbar-collapse">
      <ul class="nav navbar-nav">
         <!--<li><a href="explore_tags.php">Explore Tags</a></li>
         <li><a href="new_stack.php">Make a Stack</a></li>
         <li><a href="home.php">Dashboard</a></li>
         <li><a href="signup.php">Sign Up</a></li>-->

          <li class="<?php echo ($page_id == "explore_tags.php" ? "active" : "");?>"><a href="explore_tags.php">Explore Tags</a></li>
          <li class="<?php echo ($page_id == "new_stack.php" ? "active" : "");?>"><a href="new_stack.php">Make a Stack</a></li>
          <li class="<?php echo ($page_id == "dashboard.php" ? "active" : "");?>"><a href="dashboard.php">Dashboard</a></li>
          <li class="<?php echo ($page_id == "signup.php" ? "active" : "");?>"><a href="signup.php">Sign Up</a></li>      

          <li class="btn1 btn-success"><a href="login.php">Login</a></li><!-- ndrysho / later -->
            </ul>
       
   </div>
</nav>
     
     </div><!-- end #menu -->
  <div id="os_logo_pg"><a href="home.php"><img src="images/logo.png" width="180" height="41"  alt=""/></a></div>
  <div id="os_socmedia_bg" class="social">
  <a class="fa fa-envelope-o" href="#"></a>
  <a class="fa fa-facebook" href="#"></a>
  <a class="fa fa-twitter" href="#"></a>
  </div>
  <div id="os_srch_pg"><form id="srch_home">
  <input type="search" placeholder="Search Stacks" value="Search Stacks" onfocus="if(this.value  == 'Search Stacks') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Search Stacks'; } " />
</form></div>
  </div>