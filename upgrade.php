<?php
$page_id = 'dashboard.php';
include 'inc/header_admin.php';
?>

     
       <div class="container hero_spot"><!-- start of #hero_spot -->
       
           
      </div><!-- end of #hero_spot -->
  </div><!-- end of #onstack01 -->
  
	<div class="start_osTop_pgTop"></div>  
   <div class="start_osTop_pg"></div>
  <!--<div class="start_os"></div>-->
  
<div class="container_home my-container">
  
<div class="container_home my-container"><!-- start of #stacking/explore -->
   
</div><!-- end #stacking/explore -->

<div class="container my-container"><!-- start of #my-container -->
      <div class="row" style="min-height:600px;">
      

     

 

    <div class="row">
    



    </div><!--/row-->

<div class="panel panel-default">
    <table class="table table-bordered" id="paymentTable">
   <thead>
       <tr style="background-color:#333; padding:30px 0;">
           <th class="tableleft" style="text-transform:uppercase;" ></style> Features</th>
           <th>Free</th>
           <th>Pro</th>
           <th>Non-Profit</th>
           <th>Commertial/Brand</th>
           <th class="tableright">Broadcast</th>
       </tr>
   </thead>
   <tbody>
       <tr>
           <td style="text-align:left;" class="userptxt">Play Video </td>
           <td ><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>

          
       </tr>
       <tr>
          <td style="text-align:left;" class="userptxt01">Record video (45s)</td>
           <td><i class="fa fa-check userpro01 userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt">Record video (2 minutes) </td>
           <td><i class="fa fa-times userpro02 userpro02"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           
       </tr>
        <tr>
          <td style="text-align:left;" class="userptxt01">Record Video (4 minutes) </td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt">Add to the stack </td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt01">Add Filters </td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
          
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt">Add Frames ( basic) </td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt01">Add Frames ( Unlimited )  </td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt">Allow anomyous submissions</td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
          
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt01">Approve stacks</td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt">Edit Videos in your Stack </td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
          
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt01">Save / Load Videos </td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt">Add Custom Logo</td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt01">Allow Donations</td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt">Advanced analytics</td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt01">Delete your videos from other stacks</td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
          
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt">Ad placement<br> (add ads to any video in info box)</td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
          
       </tr>
       
       <tr>
           <td style="text-align:left;" class="userptxt01">Private stacks</td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt">Multiple user can mange a stack</td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           
       </tr>
       <tr>
           <td style="text-align:left;" class="userptxt01">License videos in stacks for broadcast<br> (50-50 rev split with user)</td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-times userpro02"></i></td>
           <td><i class="fa fa-check userpro01"></i></td>
           
       </tr>
        <tr>
           <td style="text-align:left;" class=""></td>
           <td><a href="payment.php" class="btn btn-successup">Upgrade</a></td>
           <td><a href="payment.php" class="btn btn-successup">Upgrade</a></li></td>
           <td><a href="payment.php" class="btn btn-successup">Upgrade</a></li></td>
           <td><a href="payment.php" class="btn btn-successup">Upgrade</a></li></td>
           <td><a href="payment.php" class="btn btn-successup">Upgrade</a></li></td>
           
       </tr>
   </tbody>
</table>
     </div>


      </div><!--  end of #cont.  -->
       <br>

  </div><!-- end of #my-container -->


  <!-- footer -->
 <?php include 'inc/footer_intro.php'; ?>